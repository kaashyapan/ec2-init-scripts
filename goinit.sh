#!/bin/bash
rm -r /usr/lib/go
wget -q https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz
sudo tar -C /usr/lib -xzf go1.7.4.linux-amd64.tar.gz
mkdir /home/ec2-user/go
export GOROOT=/usr/lib/go
export GOBIN=$GOROOT/bin
export GOPATH=/home/ec2-user/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

